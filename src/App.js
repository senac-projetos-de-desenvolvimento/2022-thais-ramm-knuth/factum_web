import { useMemo } from "react";
import { BrowserRouter } from "react-router-dom";

import { useUser } from "Shared/Context/userContext";
import { Theme, GlobalStyles } from "Shared/Theme";
import UnauthenticatedApp from "Routes/UnauthenticatedApp";
import AdministratorApp from "Routes/AdministratorApp";
import LeaderApp from "Routes/LeaderApp";
import LedApp from "Routes/LedApp";

const App = () => {
  const { user } = useUser();

  const Routes = useMemo(() => {
    switch (user?.role) {
      case "ROLE_ADMIN":
        return AdministratorApp;
      case "ROLE_LEADER":
        return LeaderApp;
      case "ROLE_LED":
        return LedApp;
      default:
        return UnauthenticatedApp;
    }
  }, [user]);

  return (
    <Theme>
      <GlobalStyles />
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
    </Theme>
  );
};

export default App;
