import axios from "axios";
import { getAccessToken } from "Shared/Helpers";

const options = {
  baseURL: "http://localhost:8762",
  headers: {
    Accept: "application/json",
    Content: "application/json",
  },
};

const client = axios.create(options);

client.interceptors.request.use(async ({ headers, ...config }) => {
  const token = getAccessToken();
  return {
    ...config,
    headers: {
      ...headers,
      Authorization: token && `Bearer ${token}`,
    },
  };
});

client.interceptors.response.use(
  (response) => response.data,
  (error) => error?.response?.data
);

export default client;
