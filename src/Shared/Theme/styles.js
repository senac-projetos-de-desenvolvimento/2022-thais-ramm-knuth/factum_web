const styles = {
  colors: {
    primary: "#E5E5E5", // background app
    secondary: "#C4C4C4", // background paper / numbers stopwatch
    tertiary: "#338799", // background stopwatch
    quaternary: "#000000", // font primary paper
    quinary: "#707070", // font secondary paper
  },
  fonts: {
    regular: "400",
    medium: "500",
    bold: "700",
  },
};

export default styles;
