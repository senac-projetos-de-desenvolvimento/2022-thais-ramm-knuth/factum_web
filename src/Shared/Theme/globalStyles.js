import { createGlobalStyle } from "styled-components";

const globalStyles = createGlobalStyle`
   * {
    border: 0;
    padding: 0;
    margin: 0;
    outline: none;
    font-family: 'Roboto', sans-serif;
    font-size: 16px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  button, a, li {
    cursor: pointer;
    &:disabled{
      cursor: not-allowed;
    }
  }
`;
export default globalStyles;
