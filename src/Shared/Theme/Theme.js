import { ThemeProvider } from "styled-components";

import styles from "./styles";

const Theme = ({ theme = styles, children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;
