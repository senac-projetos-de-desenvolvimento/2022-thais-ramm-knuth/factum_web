export const ACCESS_TOKEN = "access-token";
export const USER = "logged-user";
export const STOPWATCH = "isRunningStopwatch";
export const STOPWATCH_TIME = "stopwatchTime"
