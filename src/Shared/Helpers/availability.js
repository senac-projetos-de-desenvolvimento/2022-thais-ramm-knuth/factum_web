import { STOPWATCH, STOPWATCH_TIME } from "./constants";

export const setIsRunningStopwatch = (isRunningStopwatch) =>
  localStorage.setItem(STOPWATCH, isRunningStopwatch);

export const getIsRunningStopwatch = () => 
  localStorage.getItem(STOPWATCH);

export const clearIsRunningStopwatch = () => 
  localStorage.removeItem(STOPWATCH);

export const setStopwatchTime = (stopwatchTime) =>
  localStorage.setItem(STOPWATCH_TIME, stopwatchTime);

export const getStopwatchTime = () => 
  localStorage.getItem(STOPWATCH_TIME);

export const clearStopwatchTime = () => 
  localStorage.removeItem(STOPWATCH_TIME);
