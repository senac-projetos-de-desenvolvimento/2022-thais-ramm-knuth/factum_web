import { format, addMinutes } from "date-fns";

export const formatDateToApi = (date) =>
  format(
    addMinutes(new Date(date), new Date().getTimezoneOffset()),
    "yyyy-MM-dd"
  );

export const formatDateToWeb = (date) =>
  format(
    addMinutes(new Date(date), new Date().getTimezoneOffset()),
    "dd/MM/yyyy"
  );
