import { ACCESS_TOKEN, USER } from "./constants";

export const setAccessToken = (token) =>
  localStorage.setItem(ACCESS_TOKEN, token);

export const getAccessToken = () => 
  localStorage.getItem(ACCESS_TOKEN);

export const clearToken = () => 
  localStorage.removeItem(ACCESS_TOKEN);

export const setLoggedUser = (user) =>
  localStorage.setItem(USER, user);

export const getLoggedUser = () =>
  localStorage.getItem(USER);

export const clearLoggedUser = () => 
  localStorage.removeItem(USER);