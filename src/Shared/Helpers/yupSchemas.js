import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const yupShapeWithResolver = (shape) => yupResolver(yup.object().shape(shape));

export const loginSchema = yupShapeWithResolver({
  email: yup
    .string()
    .email("Insira um e-mail válido")
    .required("Insira seu e-mail"),
  pass: yup.string().min(5, "Mínimo de 5 caracteres no campo").required(),
});

export const filterByDateSchema = yupShapeWithResolver({
  date: yup
    .string()
    .matches(
      /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/,
      "Insira uma data válida"
    )
    .required(),
});

export const addTaskSchema = yupShapeWithResolver({
  description: yup.string().min(3, "Mínimo de 3 caracteres no campo"),
  categoryName: yup
    .string()
    .min(1, "Selecione a categoria")
    .required("Selecione o categoria"),
  durationTime: yup
    .string()
    .matches(/\d/, "Insira o tempo de estimado")
    .required(),
  date: yup
    .string()
    .matches(
      /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/,
      "Insira uma data válida"
    )
    .required(),
  projectName: yup
    .string()
    .min(1, "Selecione o projeto")
    .required("Selecione o projeto"),
});

export const addUserSchema = yupShapeWithResolver({
  groupName: yup
    .string()
    .min(1, "Selecione o nível de acesso")
    .required("Selecione o nível de acesso"),
  name: yup.string().min(3, "Mínimo de 3 caracteres no campo"),
  email: yup
    .string()
    .email("Insira um e-mail válido")
    .required("Insira o e-mail"),
  pass: yup.string().min(5, "Mínimo de 5 caracteres no campo").required(),
  picture: yup.string().required("Digite a URL da imagem"),
});
