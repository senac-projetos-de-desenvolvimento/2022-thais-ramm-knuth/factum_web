import React from "react";
import styled from "styled-components";
import { variant } from "styled-system";
import PropTypes from "prop-types";
import propTypes from "@styled-system/prop-types";

import { Stopwatch, Container, Image, Row } from "Shared/Components";

const HeaderComponent = ({ src, alt, ...props }) => {
  return (
    <Container mb={130}>
      <Header {...props}>
        <Row justifyContent="space-between" mx={22} mt={25}>
          <Image src={src} alt={alt} variant="regular" />
          <Stopwatch />
        </Row>
      </Header>
    </Container>
  );
};

const Header = styled.div`
  z-index: 1;
  width: 100%;
  height: 130px;
  position: fixed;
  background-color: #707070;
  /* -moz-box-shadow: inset 0 -10px 10px -10px #707070;
  -webkit-box-shadow: inset 0 -10px 10px -10px #707070;
  box-shadow: inset 0 -10px 10px -10px rgba(196, 196, 196, 0.5); */
  ${variant({
    variants: {
      light: {
        backgroundColor: "#e5e5e5",
        // boxShadow: "inset 0 -10px 10px -10px #707070",
      },
    },
  })}
`;

HeaderComponent.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  ...propTypes.variant,
};

export default HeaderComponent;
