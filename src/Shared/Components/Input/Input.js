import React, { forwardRef } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { layout, variant } from "styled-system";
import propTypes from "@styled-system/prop-types";

import { Column, Text } from "Shared/Components";

const InputComponent = forwardRef(
  ({ label, name, placeholder, error, type, IsInputDate, ...props }, ref) => (
    <Column mb={32}>
      <Column position="relative">
        {label && (
          <Text fontSize={11} position="absolute" m="5px 0px 0px 8px">
            {label}
          </Text>
        )}
        <Input
          name={name}
          placeholder={placeholder}
          type={type}
          error={error}
          IsInputDate={IsInputDate}
          ref={ref}
          {...props}
        />
        <Text variant="small" position="absolute" top={IsInputDate ? 37 : 60} color="#FF0000">
          {error}
        </Text>
      </Column>
    </Column>
  )
);

const Input = styled.input`
  width: 294px;
  height: 19px;
  padding: 25px 10px 8px 8px;
  font-size: 16px;
  border-radius: 4px;
  border: 2px solid #707070;
  background-color: #ffffff;
  ${layout}
  ${variant({
    variants: {
      InputDate: {
        width: "125px",
        height: "32px",
        color: "#707070",
        background: "#C4C4C4",
        border: "1px solid #707070",
        padding: "0px 4px 0px 9px",
      }
    },
  })}
`;

InputComponent.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  register: PropTypes.func,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  type: PropTypes.string,
  IsInputDate: PropTypes.bool,
  ...propTypes.variant,
  ...propTypes.layout,
};

export default InputComponent;
