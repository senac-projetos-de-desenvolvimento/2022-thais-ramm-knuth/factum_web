import { Row } from "Shared/Components";

const ColumnComponent = (props) => <Row flexDirection="column" {...props} />;

export default ColumnComponent;
