import styled, { css } from "styled-components";
import { variant } from "styled-system";
import PropTypes from "prop-types";
import propTypes from "@styled-system/prop-types";

const ImageComponent = ({ src, atl, isAvailability = true, ...props }) => {
  return (
    <ImageFrame {...props}>
      <Image src={src} atl={atl} isAvailability={isAvailability} />
    </ImageFrame>
  );
};

const Image = styled("img")(
  {
    width: "100%",
    height: "100%",
    borderRadius: "100%",
  },
  ({ isAvailability }) => css`
    opacity: ${isAvailability ? 1 : 0.5};
  `
);

const ImageFrame = styled.div`
  width: 70px;
  height: 70px;
  border-radius: 100%;
  background-color: #c4c4c4;
  box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.5);
  ${variant({
    variants: {
      regular: {
        width: "80px",
        height: "80px",
        boxShadow: "0px 2px 3px 0px #707070",
      },
    },
  })}
`;

ImageComponent.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  ...propTypes.color,
};

export default ImageComponent;
