import { forwardRef } from "react";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";

import { Column as ColumnDefault, Text } from "Shared/Components";

const SelectedOption = forwardRef(
  ({ label, error, isSelected, ...props }, ref) => (
    <ColumnDefault
      bg="#FFFFFF"
      width={312}
      height={52}
      position="absolute"
      color="#212121"
      borderRadius={4}
      border="2px solid #707070"
    >
      <Text fontSize={11} position="absolute" m="5px 0px 0px 8px">
        {label}
      </Text>
      <Column isSelected={isSelected}>
        <Input
          error={error}
          ref={ref}
          {...props}
        />
        <Text variant="small" position="absolute" mt={58} color="#FF0000">
          {error}
        </Text>
      </Column>
    </ColumnDefault>
  )
);
const Column = styled(ColumnDefault)(
  ({ isSelected }) => css`
    visibility: ${isSelected ? "hidden" : "visible"};
  `
);

SelectedOption.propTypes = {
  label: PropTypes.string,
  error: PropTypes.string,
  isSelected: PropTypes.bool,
  register: PropTypes.func,
};

const Input = styled.input`
  color: #212121;
  padding: 28px 0px 0px 7px;
  font-size: 16px;
  position: absolute;
`;

export default SelectedOption;
