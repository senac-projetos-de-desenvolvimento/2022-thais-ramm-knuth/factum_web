import styled from "styled-components";
import { space, layout, flexbox } from "styled-system";
import propTypes from "@styled-system/prop-types";

const ContainerComponent = ({ children, ...props }) => (
  <Container {...props}>{children}</Container>
);

const Container = styled.div`
  width: 100%;
  margin-right: auto;
  margin-left: auto;
  ${space}
  ${layout} 
  ${flexbox}
`;

ContainerComponent.propTypes = {
  ...propTypes.space,
  ...propTypes.layout,
  ...propTypes.flexbox,
};

export default ContainerComponent;
