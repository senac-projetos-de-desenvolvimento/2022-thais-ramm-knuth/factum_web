import { Link as RouterLink } from "react-router-dom";
import styled from "styled-components";
import { typography, color, space , flexbox} from "styled-system";
import PropTypes from "prop-types";
import propTypes from "@styled-system/prop-types";

const LinkComponent = ({ to, label, children, ...props }) => (
  <Link to={to} {...props}>
    {label || children}
  </Link>
);

const Link = styled(RouterLink)`
  display: flex;
  text-decoration: none;
  color: #212121;
  ${typography}
  ${color}
  ${space}
  ${flexbox}
`;

LinkComponent.propTypes = {
  to: PropTypes.string,
  label: PropTypes.string,
  ...propTypes.typography,
  ...propTypes.color,
  ...propTypes.space,
  ...propTypes.flexbox,
};

export default LinkComponent;
