import styled from "styled-components";
import {
  WorkHistorySharp,
  GroupsSharp,
  PlaylistAddCheckSharp,
  BarChartSharp,
  Logout,
} from "@mui/icons-material";

import { Column, Link, Text, Container } from "Shared/Components";
import { useUser } from "Shared/Context/userContext";

const BottomNavigationComponent = () => {
  const { user, logout } = useUser();
  return (
    <Container mb={50}>
      <BottomNavigation>
        <Column alignItems="center" ml={22}>
          <Link to={"/registros/cria"}>
            <WorkHistorySharp sx={{ fontSize: 20 }} />
          </Link>
          <Text fontSize="12px">Registrar</Text>
        </Column>
        <Column alignItems="center">
          <Link to={"/usuarios/lista"}>
            <GroupsSharp sx={{ fontSize: 25 }} />
          </Link>
          <Text fontSize="12px">Pessoas</Text>
        </Column>
        <Column alignItems="center">
          <Link to={"/registros/lista"}>
            <PlaylistAddCheckSharp sx={{ fontSize: 27 }} />
          </Link>
          <Text fontSize="12px">Lista</Text>
        </Column>
        {user.role === "ROLE_LEADER" && (
          <Column alignItems="center">
            <Link to={"/graficos/projetos"}>
              <BarChartSharp sx={{ fontSize: 25 }} />
            </Link>
            <Text fontSize="12px">Gráficos</Text>
          </Column>
        )}
        <Column alignItems="center" mr={22}>
          <Link to={"/login"} onClick={logout} variant="transparent">
            <Logout sx={{ fontSize: 20 }} />
          </Link>
          <Text fontSize="12px">Sair</Text>
        </Column>
      </BottomNavigation>
    </Container>
  );
};

const BottomNavigation = styled.div`
  z-index: 1;
  display: flex;
  width: 100%;
  height: 50px;
  position: fixed;
  bottom: 0px;
  background-color: #338799;
  color: #212121;
  align-items: center;
  justify-content: space-between;
`;

export default BottomNavigationComponent;
