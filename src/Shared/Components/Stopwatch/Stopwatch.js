import React, { useEffect, useState } from "react";
import styled from "styled-components";
import RefreshIcon from "@mui/icons-material/Refresh";

import { Clock } from "./Clock";
import {
  setIsRunningStopwatch,
  getIsRunningStopwatch,
  clearIsRunningStopwatch,
  getStopwatchTime,
  clearStopwatchTime,
} from "Shared/Helpers";
import { useUser } from "Shared/Context/userContext";

const Stopwatch = (isCounting) => {
  const { updateUserAvailability } = useUser();

  let [time, setTime] = useState(1);
  let [isRunning, setIsRunning] = useState(isCounting | false);
  
  let timeSavedOnLocalStorage = Number(getStopwatchTime() || 0);
  let isRunningLocalStorage = getIsRunningStopwatch();

  const startOrStopTimer = () => {
    if (isRunning) {
      if (timeSavedOnLocalStorage > 0) setTime(timeSavedOnLocalStorage);
      setIsRunningStopwatch(String("false"));
      setIsRunning(false);
      updateUserAvailability(false)
    } else {
      setIsRunningStopwatch(String("true"));
      setIsRunning(true);
      updateUserAvailability(true)
    }
  };

  const resetTimer = () => {
    clearIsRunningStopwatch();
    clearStopwatchTime();
    setTime(0);
    setIsRunning(false);
  };

  useEffect(() => {
    let interval = 0;

    if (timeSavedOnLocalStorage > 0) setTime(timeSavedOnLocalStorage);

    if (isRunning || isRunningLocalStorage === "true") {
      interval = setInterval(() => {
        setTime((num) => {
          localStorage.setItem("stopwatchTime", String(num + 1));
          return (num += 1);
        });
      }, 1000);
    } else if (!isRunning) {
      setTime(timeSavedOnLocalStorage);
      return clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isRunning]);

  return (
    <Div>
      <TimerButton onClick={() => startOrStopTimer()}>
        <Clock time={time} />
      </TimerButton>
      <ReloadButton onClick={() => resetTimer()}>
        <RefreshIcon />
      </ReloadButton>
    </Div>
  );
};

const Div = styled.div`
  display: flex;
  align-items: center;
`;

const TimerButton = styled.button`
  width: 123px;
  height: 34px;
  border: none;
  box-shadow: 0px 2px 3px 0px #707070;
  background: ${({ theme }) => theme.colors.tertiary};
`;

const ReloadButton = styled.button`
  height: 34px;
  width: 40px;
  margin-left: 5px;
  padding: 4px 1px 1px 1px;
  border: none;
  box-shadow: 0px 2px 3px 0px #707070;
  color: ${({ theme }) => theme.colors.secondary};
  background: ${({ theme }) => theme.colors.tertiary};
`;

export default Stopwatch;

// import React, { useEffect, useState } from "react";
// import styled from "styled-components";
// import RefreshIcon from "@mui/icons-material/Refresh";

// import { Clock } from "./Clock";
// import { setIsRunningStopwatch, getIsRunningStopwatch } from "Shared/Helpers";

// const Stopwatch = () => {
//   let [time, setTime] = useState(1);
//   let [isRunning, setIsRunning] = useState(false);
//   let timeSavedOnLocalStorage = Number( localStorage.getItem("stopwatchTime") || 0 );
//   let isRunningLocalStorage = getIsRunningStopwatch();

//   const startOrStopTimer = () => {
//     if (isRunning) {
//       if (timeSavedOnLocalStorage > 0) setTime(timeSavedOnLocalStorage);
//       setIsRunningStopwatch(String("false"));
//       setIsRunning(false);
//     } else {
//       setIsRunningStopwatch(String("true"));
//       setIsRunning(true);
//     }
//   };

//   const resetTimer = () => {
//     localStorage.clear();
//     setTime(0);
//     setIsRunning(false);
//   };

//   useEffect(() => {
//     let interval = 0;

//     if (timeSavedOnLocalStorage > 0) setTime(timeSavedOnLocalStorage);

//     if (isRunning || isRunningLocalStorage === "true") {
//       interval = setInterval(() => {
//         setTime((num) => {
//           localStorage.setItem("stopwatchTime", String(num + 1));
//           return (num += 1);
//         });
//       }, 1000);
//     } else if (!isRunning) {
//       setTime(timeSavedOnLocalStorage);
//       return clearInterval(interval);
//     }
//     return () => clearInterval(interval);
//   }, [isRunning]);

//   return (
//     <Div>
//       <TimerButton onClick={() => startOrStopTimer()}>
//         <Clock time={time} />
//       </TimerButton>
//       <ReloadButton onClick={() => resetTimer()}>
//         <RefreshIcon />
//       </ReloadButton>
//     </Div>
//   );
// };

// const Div = styled.div`
//   display: flex;
//   align-items: center;
// `;

// const TimerButton = styled.button`
//   width: 123px;
//   height: 34px;
//   border: none;
//   box-shadow: 0px 2px 3px 0px #707070;
//   background: ${({ theme }) => theme.colors.tertiary};
// `;

// const ReloadButton = styled.button`
//   height: 34px;
//   width: 40px;
//   margin-left: 5px;
//   padding: 4px 1px 1px 1px;
//   border: none;
//   box-shadow: 0px 2px 3px 0px #707070;
//   color: ${({ theme }) => theme.colors.secondary};
//   background: ${({ theme }) => theme.colors.tertiary};
// `;

// export default Stopwatch;
