import React from "react";
import styled from "styled-components";

const Clock = ({ time = 0 }) => {
  const hours = Math.floor(time / 3600);
  const minutes = Math.floor((time - hours * 3600) / 60);
  const seconds = time - hours * 3600 - minutes * 60;
  const [hourDonzes, hourUnit] = String(hours).padStart(2, "0");
  const [minuteDonzes, minuteUnit] = String(minutes).padStart(2, "0");
  const [secondDonzes, secondUnit] = String(seconds).padStart(2, "0");

  return (
    <React.Fragment>
      <Div>
        <span>{hourDonzes}</span>
        <span>{hourUnit}</span>
        <span>:</span>
        <span>{minuteDonzes}</span>
        <span>{minuteUnit}</span>
        <span>:</span>
        <span>{secondDonzes}</span>
        <span>{secondUnit}</span>
      </Div>
    </React.Fragment>
  );
};

const Div = styled.div`
  color: ${({ theme }) => theme.colors.secondary};
`;

export default Clock;
