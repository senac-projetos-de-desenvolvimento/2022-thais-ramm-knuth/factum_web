import styled from "styled-components";
import { space, color, layout, position, variant } from "styled-system";
import propTypes from "@styled-system/prop-types";

const ButtonComponent = ({ children, ...props }) => (
  <Button {...props}>{children}</Button>
);

const Button = styled.button`
  width: 176px;
  height: 40px;
  background-color: #338799;
  color: #FFFFFF;
  font-weight: 600;
  font-size: 14px;
  border-radius: 5px;
  border: 1px solid #707070;
  ${space}
  ${color}
  ${layout}
  ${position}
  ${variant({
    variants: {
      transparent: {
        width: '0px',
        height: '0px',
        border: 'none',
        backgroundColor: 'transparent',
      },
      small: {
        width: '115px'
      }
    }
  })}
`;

ButtonComponent.propTypes = {
  ...propTypes.space,
  ...propTypes.layout,
  ...propTypes.position,
  ...propTypes.variant,
};

export default ButtonComponent;
