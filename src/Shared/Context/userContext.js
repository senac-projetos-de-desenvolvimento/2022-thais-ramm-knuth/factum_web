import { createContext, useContext, useEffect, useState } from "react";
import { useMutation } from "react-query";

import {
  login as loginService,
  getUser,
  updateAvailability,
} from "Shared/Services/users";
import {
  setAccessToken,
  getAccessToken,
  clearToken,
  setLoggedUser,
  clearLoggedUser,
} from "Shared/Helpers";

const UserContext = createContext();

const useUser = () => {
  const context = useContext(UserContext);

  if (context === undefined) {
    throw new Error("useUser must be uIsed within a UserProvider");
  }

  return context;
};

const UserProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [userPhoto, setUserPhoto] = useState();

  useEffect(() => {
    const fetchUser = async () => {
      const token = getAccessToken();

      if (token) {
        const { id, email, picture, groups, availabilityStatus } =
          await getUser();
        const user = {
          id: id,
          email: email,
          role: groups[0]?.name,
          availabilityStatus: availabilityStatus,
        };
        setUser(user);
        setUserPhoto(picture);
      }
    };

    fetchUser();
  }, []);

  const { mutate: loginContext } = useMutation(loginService, {
    onSuccess: async ({ token }) => {
      setAccessToken(token);
      const { id, email, picture, groups } = await getUser();
      const user = { id: id, email: email, role: groups[0]?.name };
      setLoggedUser(JSON.stringify(user));
      setUser(user);

      setUserPhoto(picture);
    },
  });

  const logout = () => {
    setUser(null);
    clearToken();
    clearLoggedUser();
  };

  const updateUserAvailability = async (availabilityStatus) =>
    await updateAvailability(availabilityStatus);

  return (
    <UserContext.Provider
      value={{ user, userPhoto, loginContext, logout, updateUserAvailability }}
    >
      {children}
    </UserContext.Provider>
  );
};

export { useUser, UserProvider };
