import { client } from "Shared/Providers";

export const getAllGroups = async () => {
  try {
    const response = await client.get("/factum-auth-api/grupos");
    return response;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};
