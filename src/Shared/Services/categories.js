import { client } from "Shared/Providers";

export const getAllCategories = async () => {
  try {
    const response = await client.get("/factum-api/categorias");
    return response;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};
