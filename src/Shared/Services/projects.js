import { client } from "Shared/Providers";

export const getAllProjects = async () => {
  try {
    const response = await client.get("/factum-api/projetos");
    return response;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};
