import { client } from "Shared/Providers";

export const getUser = () =>
  client
    .get("/factum-auth-api/auth/me")
    .then((response) => response)
    .catch((error) => console.log(`Error ${error}`));

export const login = async (data) => {
  try {
    const response = await client.post("/factum-auth-api/auth", data);
    return response;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const createUser = async (data) => {
  try {
    await client.post("/factum-auth-api/usuarios", data);
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const getAllUsers = async () => {
  try {
    const response = await client.get("/factum-auth-api/usuarios");
    return response;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const getOneUser = async (id) => {
  try {
    const response = await client.get(`/factum-auth-api/usuarios/id/${id}`);
    return response;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const updateUser = async ({ id, ...data }) => {
  try {
    await client.put(`/factum-auth-api/usuarios/${id}`, data);
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const deleteUser = async (id) => {
  try {
    await client.delete(`/factum-auth-api/usuarios/${id}`);
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const updateAvailability = async (Status) => {
  try {
    const { id } = await getUser();
    const { groups, availabilityStatus, ...user } = await getOneUser(id);
    const data = {
      availabilityStatus: Status,
      groupName: groups[0].name,
      ...user,
    };
    await updateUser({ id, ...data });
  } catch (error) {
    console.log(`Error ${error}`);
  }
};
