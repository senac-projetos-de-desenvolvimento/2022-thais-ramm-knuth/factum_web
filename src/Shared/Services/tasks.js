import { client } from "Shared/Providers";
import { formatDateToApi, formatDateToWeb } from "Shared/Helpers";

export const createTask = async ({ date, ...data }) => {
  try {
    await client.get("/factum-api/lista/usuario");
    const task = { date: formatDateToApi(date), ...data };
    await client.post("/factum-api/lista", task);
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const getAllTasks = async (id) => {
  try {
    const response = await client.get(`/factum-api/lista/usuario/${id}`);
    return response.content;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const getOneTasks = async (id) => {
  try {
    const { executionDate, ...data } = await client.get(
      `/factum-api/lista/${id}`
    );
    const task = { executionDate: formatDateToWeb(executionDate), ...data };
    return task;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const updateTask = async ({ id, date, ...data }) => {
  try {
    const task = { date: formatDateToApi(date), ...data };
    await client.put(`/factum-api/lista/${id}`, task);
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const deleteTask = async (id) => {
  try {
    await client.delete(`/factum-api/lista/${id}`);
  } catch (error) {
    console.log(`Error ${error}`);
  }
};

export const getAllTasksByDate = async (id, { date }) => {
  const formattedFilterDate = formatDateToApi(date);
  let tasksByDate = [];
  const allTasks = await getAllTasks(id);

  allTasks.map((task) => {
    if (task.executionDate === formattedFilterDate) {
      tasksByDate.push(task);
    }
    return tasksByDate;
  });

  return tasksByDate;
};

export const getGeneralTasks = async () => {
  try {
    const response = await client.get("/factum-api/lista");
    return response.content;
  } catch (error) {
    console.log(`Error ${error}`);
  }
};