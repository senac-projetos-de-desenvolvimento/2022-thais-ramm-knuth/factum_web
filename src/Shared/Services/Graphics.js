import { getAllProjects } from "./projects";
import { getGeneralTasks } from "./tasks";
import { getAllCategories } from "./categories";

export const hoursPerProjects = async () => {
  const projects = await getAllProjects();
  const tasks = await getGeneralTasks();
  let data = [["Project", "Hours per Project"]];

  projects.map((project) => {
    let projectName = project.name;
    let hours = 0;

    tasks.map((task) => {
      if (task.project.name === projectName) {
        hours += task.durationTime;
      }
      return hours;
    });

    return data.push([projectName, hours]);
  });

  return data;
};

export const hoursPerProjectCategory = async (project) => {
  const categories = await getAllCategories();
  const tasks = await getGeneralTasks();
  let projectName = project;
  let data = [["Category", "Hours per Category"]];

  categories.map((category) => {
    let categoryName = category.name;
    let hours = 0;

    tasks.map((task) => {
      if (
        task.project.name === projectName &&
        task.category.name === categoryName
      ) {
        hours += task.durationTime;
      }
      return hours;
    });

    return data.push([categoryName, hours]);
  });

  return data;
};
