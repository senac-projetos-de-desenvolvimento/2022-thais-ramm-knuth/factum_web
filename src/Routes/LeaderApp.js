import { Routes, Route, Navigate } from "react-router-dom";

import {
  TaskListContainer,
  TaskFormContainer,
  UsersListContainer,
  ProjectsGraphicContainer,
  CategoriesGraphicContainer,
} from "Pages";

const AuthenticatedApp = () => {
  return (
    <Routes>
      <Route path="/registros/lista" element={<TaskListContainer />} />
      <Route path="/registros/cria" element={<TaskFormContainer />} />
      <Route path="/registros/edita/:id" element={<TaskFormContainer />} />

      <Route path="/usuarios/lista" element={<UsersListContainer />} />

      <Route path="/graficos/projetos" element={<ProjectsGraphicContainer />} />
      <Route path="/graficos/categorias" element={<CategoriesGraphicContainer />} />

      <Route path="*" element={<Navigate to={"/registros/lista"} />} />
    </Routes>
  );
};

export default AuthenticatedApp;
