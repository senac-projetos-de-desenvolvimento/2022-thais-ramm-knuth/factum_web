import { Routes, Route, Navigate } from "react-router-dom";

import {
  TaskListContainer,
  TaskFormContainer,
  UsersListContainer,
} from "Pages";

const AuthenticatedApp = () => {
  return (
    <Routes>
      <Route path="/registros/lista" element={<TaskListContainer />} />
      <Route path="/registros/cria" element={<TaskFormContainer />} />
      <Route path="/registros/edita/:id" element={<TaskFormContainer />} />

      <Route path="/usuarios/lista" element={<UsersListContainer />} />

      <Route path="*" element={<Navigate to={"/registros/lista"} />} />
    </Routes>
  );
};

export default AuthenticatedApp;
