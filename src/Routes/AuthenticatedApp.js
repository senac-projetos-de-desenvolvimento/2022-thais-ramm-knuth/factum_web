import { Routes, Route, Navigate } from "react-router-dom";

import {
  TaskFormContainer,
  TaskListContainer,
  ActiveContributorListContainer,
} from "Pages";

const AuthenticatedApp = () => {
  return (
    <Routes>
      <Route path={"/listagem"} element={<TaskListContainer />} />
      <Route path="/registro" element={<TaskFormContainer />} />
      <Route path="/registro/:id" element={<TaskFormContainer />} />
      <Route path="/usuarios" element={<ActiveContributorListContainer />} />

      <Route path="*" element={<Navigate to={"/listagem"} />} />
    </Routes>
  );
};

export default AuthenticatedApp;
