import { Routes, Route, Navigate } from "react-router-dom";

import {
  TaskListContainer,
  TaskFormContainer,
  UsersListContainer,
  UserFormContainer,
  UserManagementContainer,
} from "Pages";

const AdministratorApp = () => {
  return (
    <Routes>
      <Route path="/registros/lista" element={<TaskListContainer />} />
      <Route path="/registros/cria" element={<TaskFormContainer />} />
      <Route path="/registros/edita/:id" element={<TaskFormContainer />} />

      <Route path="/usuarios/lista" element={<UsersListContainer />} />

      <Route path="/admin/usuarios/cria" element={<UserFormContainer />} />
      <Route path="/admin/usuarios/edita/:id" element={<UserFormContainer />} />
      <Route path="/admin/usuarios/gerencia" element={<UserManagementContainer />} />

      <Route path="*" element={<Navigate to={"/registros/lista"} />} />
    </Routes>
  );
};

export default AdministratorApp;
