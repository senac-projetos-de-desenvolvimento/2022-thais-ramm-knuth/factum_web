import { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";
import { useParams, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import AddIcon from "@mui/icons-material/Add";
import ListIcon from "@mui/icons-material/List";
import { Select, MenuItem } from "@mui/material";
import { EditSharp } from "@mui/icons-material";

import {
  Column,
  Text,
  Header,
  Row,
  Input,
  Loader,
  SelectedOption,
  Button,
  Link,
  BottomNavigation,
} from "Shared/Components";
import { useUser } from "Shared/Context/userContext";
import { useModal } from "Shared/Context/modalContext";
import { getOneUser, createUser, updateUser } from "Shared/Services/users";
import { getAllGroups } from "Shared/Services/groups";
import { addUserSchema } from "Shared/Helpers/yupSchemas";

const UserFormContainer = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const { handleOpenModal } = useModal();
  const { userPhoto } = useUser();

  const { data: groups } = useQuery(["groups"], getAllGroups);
  const { data: user, isFetching } = useQuery(
    ["user", id],
    async () => await getOneUser(id),
    {
      enabled: !!id,
    }
  );
  
  const [group, setGroup] = useState();
  const [selectedGroup, setSelectedGroup] = useState(false);

  const { mutate: create } = useMutation(createUser, {
    onSuccess: () => {
      handleOpenModal({
        type: "success",
        title: "Usuário criado",
        content: "Usuário criado com sucesso!",
        onClose: () => navigate("/admin/usuarios/gerencia"),
      });
    },
  });
  const { mutate: update } = useMutation(updateUser, {
    onSuccess: () => {
      handleOpenModal({
        type: "success",
        title: "Usuário atualizado",
        content: "Usuário atualizado com sucesso!",
        onClose: () => navigate("/admin/usuarios/gerencia"),
      });
    },
  });

  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: addUserSchema });

  const onSubmit = (data) => (id ? update({ id: id, ...data }) : create(data));

  const handleSelectGroup = ({ target }) => {
    reset({ groupName: "" });
    setGroup(target.value);
    setSelectedGroup(true);
  };

  useEffect(() => {
    reset({
      groupName: user?.groups[0]?.name || "",
      name: user?.name || "",
      email: user?.email || "",
      pass: user?.pass || "",
      picture: user?.picture || "",
    });
  }, [user, reset]);

  return (
    <Column bg="#808080" color="#c4c4c4" height="120vh" alignItems="center">
      <Header src={userPhoto} alt="Foto do usuario" />
      <Row
        width={312}
        mt={29}
        mb={20}
        alignItems="center"
        justifyContent="space-between"
      >
        {id ? (
          <Link to={""} color="#c4c4c4">
            <EditSharp sx={{ fontSize: 20 }} />
            <Text fontWeight={600} mt="3px">
              Atualizar
            </Text>
          </Link>
        ) : (
          <Link to={""} color="#c4c4c4">
            <AddIcon sx={{ fontSize: 25 }} />
            <Text fontWeight={600} mt="3px">
              Novo
            </Text>
          </Link>
        )}
        <Text>|</Text>
        <Link to={"/admin/usuarios/gerencia"} color="#c4c4c4">
          <ListIcon sx={{ fontSize: 25 }} />
          <Text mt="3px" ml="2px">
            Todos
          </Text>
        </Link>
      </Row>
      {isFetching ? (
        <Loader />
      ) : (
        <Column as="form" onSubmit={handleSubmit(onSubmit)} alignItems="center">
          <Column mb={32} alignItems="center">
            <SelectedOption
              label="Nível de acesso"
              {...register("groupName")}
              error={errors.groupName?.message}
              isSelected={selectedGroup}
              disabled="disabled"
            />
            <Select
              sx={{
                borderRadius: 0,
                width: "298px",
                m: "23.5px 0px 0px 0px",
              }}
              value={group}
              variant="standard"
              {...register("groupName")}
              onChange={handleSelectGroup}
            >
              {groups?.map(({ id, name }) => (
                <MenuItem key={id} value={name}>
                  {name}
                </MenuItem>
              ))}
            </Select>
          </Column>

          <Input
            name="name"
            label="Nome"
            placeholder="Nome completo"
            type="text"
            ref="ref"
            {...register("name")}
            error={errors.name?.message}
          />
          <Input
            name="email"
            label="E-mail"
            placeholder="exemplo@domínio.com"
            type="text"
            ref="ref"
            {...register("email")}
            error={errors.email?.message}
          />
          <Input
            name="pass"
            label="Senha"
            placeholder="*********"
            type="password"
            ref="ref"
            {...register("pass")}
            error={errors.pass?.message}
          />
          <Input
            name="picture"
            label="Foto"
            placeholder="URL da imagem"
            type="text"
            ref="ref"
            {...register("picture")}
            error={errors.picture?.message}
          />
          <Row width={120} height={50}>
            <Button
              onClick={() => navigate("/usuarios/lista")}
              color="#c4c4c4"
              variant="transparent"
            >
              <ArrowBackIcon sx={{ fontSize: 45 }} />
            </Button>
            <Button type="submit" color="#c4c4c4" variant="transparent" ml={70}>
              <CheckCircleOutlineIcon sx={{ fontSize: 45 }} />
            </Button>
          </Row>
        </Column>
      )}
      <BottomNavigation />
    </Column>
  );
};

export default UserFormContainer;
