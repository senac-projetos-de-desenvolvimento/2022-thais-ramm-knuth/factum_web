import { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";
import { useParams, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Select, MenuItem } from "@mui/material";

import {
  Row,
  Column,
  Header,
  Loader,
  Text,
  Input,
  SelectedOption,
  Button,
  BottomNavigation,
} from "Shared/Components";
import { updateTask, createTask, getOneTasks } from "Shared/Services/tasks";
import { getAllCategories } from "Shared/Services/categories";
import { getAllProjects } from "Shared/Services/projects";
import { addTaskSchema } from "Shared/Helpers/yupSchemas";
import { useModal } from "Shared/Context/modalContext";
import { useUser } from "Shared/Context/userContext";

const TaskFormContainer = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  
  const { handleOpenModal } = useModal();
  const { userPhoto } = useUser();

  const { data: categories } = useQuery(["categories"], getAllCategories);
  const { data: projects } = useQuery(["projects"], getAllProjects);
  const { data: task, isFetching } = useQuery(
    ["task", id],
    async () => await getOneTasks(id),
    {
      enabled: !!id,
    }
  );

  const [category, setCategory] = useState();
  const [project, setProject] = useState();
  const [selectedCategory, setSelectedCategory] = useState(false);
  const [selectedProject, setSelectedProject] = useState(false);

  const { mutate: create } = useMutation(createTask, {
    onSuccess: () => {
      handleOpenModal({
        type: "success",
        title: "Registro criado",
        content: "Registro criado com sucesso!",
        onClose: () => navigate("/registros/lista"),
      });
    },
  });
  const { mutate: update } = useMutation(updateTask, {
    onSuccess: () => {
      handleOpenModal({
        type: "success",
        title: "Registro atualizado",
        content: "Registro atualizado com sucesso!",
        onClose: () => navigate("/registros/lista"),
      });
    },
  });

  const {
    reset,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: addTaskSchema });

  const onSubmit = (data) => (id ? update({ id: id, ...data }) : create(data));

  const handleSelectCategory = ({ target }) => {
    reset({ categoryName: "" });
    setCategory(target.value);
    setSelectedCategory(true);
  };

  const handleSelectProject = ({ target }) => {
    reset({ projectName: "" });
    setProject(target.value);
    setSelectedProject(true);
  };

  useEffect(() => {
    reset({
      description: task?.description || "",
      categoryName: task?.category?.name || "",
      durationTime: task?.durationTime || "",
      date: task?.executionDate || "",
      projectName: task?.project?.name || "",
    });
  }, [task, reset]);

  return (
    <Column bg="#C4C4C4" color="#707070" height="110vh" alignItems="center">
      <Header src={userPhoto} alt="Foto do usuario" variant="light" />
      <Text mt={29} mb={20}>
        {id ? "Atualizar Registro" : "Criar Registro"}
      </Text>
      {isFetching ? (
        <Loader />
      ) : (
        <Column as="form" onSubmit={handleSubmit(onSubmit)} alignItems="center">
          <Input
            name="description"
            label="Descrição"
            placeholder="Descreva aqui sua tarefa"
            type="text"
            {...register("description")}
            error={errors.description?.message}
            width={296}
            height="auto"
          />
          <Column mb={32} alignItems="center">
            <SelectedOption
              label="Categoria"
              {...register("categoryName")}
              error={errors.categoryName?.message}
              isSelected={selectedCategory}
              disabled="disabled"
            />
            <Select
              sx={{
                borderRadius: 0,
                width: "298px",
                m: "23.5px 0px 0px 0px",
              }}
              value={category}
              variant="standard"
              {...register("categoryName")}
              onChange={handleSelectCategory}
            >
              {categories?.map(({ id, name }) => (
                <MenuItem key={id} value={name}>
                  {name}
                </MenuItem>
              ))}
            </Select>
          </Column>
          <Input
            name="durationTime"
            label="Tempo de duração"
            placeholder="Digite o número de horas"
            type="number"
            {...register("durationTime")}
            error={errors.durationTime?.message}
          />
          <Input
            name="date"
            label="Data de realização"
            placeholder="dd/mm/aaaa"
            type="string"
            {...register("date")}
            error={errors.date?.message}
          />
          <Column mb={22} alignItems="center">
            <SelectedOption
              label="Projeto"
              {...register("projectName")}
              error={errors.projectName?.message}
              isSelected={selectedProject}
              disabled="disabled"
            />
            <Select
              sx={{
                borderRadius: 0,
                width: "298px",
                m: "23.5px 0px 0px 0px",
              }}
              value={project}
              variant="standard"
              {...register("projectName")}
              onChange={handleSelectProject}
            >
              {projects?.map(({ id, name }) => (
                <MenuItem key={id} value={name}>
                  {name}
                </MenuItem>
              ))}
            </Select>
          </Column>
          <Row width={120} height={50} mb={63}>
            <Button
              onClick={() => navigate("/registros/lista")}
              color="#808080"
              variant="transparent"
            >
              <ArrowBackIcon sx={{ fontSize: 45 }} />
            </Button>
            <Button type="submit" color="#808080" variant="transparent" ml={70}>
              <CheckCircleOutlineIcon sx={{ fontSize: 45 }} />
            </Button>
          </Row>
        </Column>
      )}
      <BottomNavigation />
    </Column>
  );
};

export default TaskFormContainer;
