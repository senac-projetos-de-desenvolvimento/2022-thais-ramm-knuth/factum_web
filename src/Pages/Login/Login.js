import React from "react";
import { useForm } from "react-hook-form";
import { useMutation } from "react-query";

import { Column, Text, Input, Button, Row } from "Shared/Components";
import { useUser } from "Shared/Context/userContext";
import { loginSchema } from "Shared/Helpers/yupSchemas";

const Login = () => {
  const { loginContext } = useUser();

  const { mutate: login, isError } = useMutation(loginContext);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: loginSchema });

  return (
    <Column bg="#C4C4C4" alignItems="center" width="100%" height="100vh">
      <Text variant="big" fontWeight={600} mt={132} mb={20}>
        Entrar
      </Text>

      {isError && (
        <Row  position="absolute" fontSize={18} color="red" mt={10}>
          <Text>E-mail e/ou senha inválido(s)</Text>
        </Row>
      )}
      <Column
        as="form"
        onSubmit={handleSubmit(login)}
        alignItems="center"
        mt={22}
      >
        <Input
          name="email"
          label="E-mail"
          placeholder="exemplo@domínio.com"
          type="email"
          ref="ref"
          {...register("email")}
          error={errors.email?.message}
        />
        <Input
          name="pass"
          label="Senha"
          placeholder="*********"
          type="password"
          ref="ref"
          {...register("pass")}
          error={errors.pass?.message}
        />
        <Button type="submit" mt={12}>
          Entrar
        </Button>
      </Column>
    </Column>
  );
};

export default Login;
