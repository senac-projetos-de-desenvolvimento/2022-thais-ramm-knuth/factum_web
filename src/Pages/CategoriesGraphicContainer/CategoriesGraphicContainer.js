import { useState } from "react";
import { Chart } from "react-google-charts";
import { useQuery } from "react-query";
import { MenuItem, Select } from "@mui/material";

import {
  Column,
  Header,
  BottomNavigation,
  Link,
  Text,
  Row,
  SelectedOption,
} from "Shared/Components";
import { useUser } from "Shared/Context/userContext";
import { hoursPerProjectCategory } from "Shared/Services/Graphics";
import { getAllProjects } from "Shared/Services/projects";

const ProjectsGraphicContainer = () => {
  const { userPhoto } = useUser();

  const [data, setData] = useState(null);
  const [project, setProject] = useState();
  const [selectedProject, setSelectedProject] = useState(false);

  const { data: projects } = useQuery(["projects"], getAllProjects);

  const fetchChartDataByProject = async (project) => {
    const chartData = await hoursPerProjectCategory(project);
    if (chartData) setData(chartData);
  };

  const handleSelectProject = ({ target }) => {
    setProject(target.value);
    fetchChartDataByProject(target.value);
    setSelectedProject(true);
  };

  const options = {
    width: "100%",
    height: 200,
    backgroundColor: "#808080",
  };

  return (
    <Column
    bg="#808080"
    color="#c4c4c4"
      height="100vh"
      overflow="auto"
      alignItems="center"
    >
      <Header src={userPhoto} alt="Foto do usuario" />
      <Row
        width={312}
        mt={29}
        alignItems="center"
        justifyContent="space-between"
      >
        <Link to={"/graficos/projetos"} color="#c4c4c4">
          <Text fontSize="14px">Horas / Projeto</Text>
        </Link>
        <Text>|</Text>
        <Link to={""} color="#c4c4c4">
          <Text fontSize="15px" fontWeight={500}>
            Horas / categoria
          </Text>
        </Link>
      </Row>
      <Text
        fontSize="18px"
        fontWeight={600}
        color="#c4c4c4"
        width={312}
        mt={22}
        textAlign="center"
      >
        Porcentagem de horas em categorias por projeto
      </Text>
      <Column mb={22} alignItems="center" mt={22}>
        <SelectedOption
          label="Projeto"
          isSelected={selectedProject}
          disabled="disabled"
        />
        <Select
          sx={{
            borderRadius: 0,
            width: "298px",
            m: "23.5px 0px 0px 0px",
          }}
          value={project}
          variant="standard"
          onChange={handleSelectProject}
        >
          {projects?.map(({ id, name }) => (
            <MenuItem key={id} value={name}>
              {name}
            </MenuItem>
          ))}
        </Select>
      </Column>
      {data && <Chart chartType="PieChart" options={options} data={data} />}
      <BottomNavigation />
    </Column>
  );
};

export default ProjectsGraphicContainer;
