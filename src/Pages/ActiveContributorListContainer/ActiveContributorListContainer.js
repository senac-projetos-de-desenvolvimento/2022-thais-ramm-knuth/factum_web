import { useQuery } from "react-query";

import {
  Column,
  Row,
  Header,
  Loader,
  Text,
  Image,
  BottomNavigation,
} from "Shared/Components";
import { useUser } from "Shared/Context/userContext";
import { getAllUsers } from "Shared/Services/users";

const ActiveContributorListContainer = () => {
  const { userPhoto } = useUser();

  const { data: users, isFetching } = useQuery(
    ["users"],
    async () => await getAllUsers()
  );

  return (
    <Column
      bg="#808080"
      color="#c4c4c4"
      height="100vh"
      overflow="auto"
      alignItems="center"
    >
      <Header src={userPhoto} alt="Foto do usuario" />
      <Text mt={29} mb={20}>
        Usuários Disponíveis
      </Text>
      {isFetching ? (
        <Loader />
      ) : (
        <Row width={296} flexWrap="wrap" gridColumnGap={43}>
          {users?.map(({ id, name, picture, availabilityStatus, ...rest }) => (
            <Column key={id} mb={12}>
              <Image
                src={picture}
                alt={`Foto do usuário ${name}`}
                isOnline={availabilityStatus}
              />
            </Column>
          ))}
        </Row>
      )}
      <BottomNavigation />
    </Column>
  );
};

export default ActiveContributorListContainer;
