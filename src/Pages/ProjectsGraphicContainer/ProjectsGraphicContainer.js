import { useState, useEffect } from "react";
import { Chart } from "react-google-charts";

import { useUser } from "Shared/Context/userContext";
import {
  Column,
  Header,
  BottomNavigation,
  Link,
  Text,
  Row,
} from "Shared/Components";
import { hoursPerProjects } from "Shared/Services/Graphics";

const ProjectsGraphicContainer = () => {
  const { userPhoto } = useUser();

  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchChartData = async () => {
      const chartData = await hoursPerProjects();
      if (chartData) setData(chartData);
    };
    fetchChartData();
  }, []);

  const options = {
    width: "100%",
    height: 300,
    backgroundColor: "#808080",
  };

  return (
    <Column
      bg="#808080"
      color="#c4c4c4"
      height="100vh"
      overflow="auto"
      alignItems="center"
    >
      <Header src={userPhoto} alt="Foto do usuario" />
      <Row
        width={312}
        mt={29}
        alignItems="center"
        justifyContent="space-between"
      >
        <Link to={""} color="#c4c4c4">
          <Text fontSize="15px" fontWeight={600}>
            Horas / Projeto
          </Text>
        </Link>
        <Text>|</Text>
        <Link to={"/graficos/categorias"} color="#c4c4c4">
          <Text fontSize="14px">Horas / categoria</Text>
        </Link>
      </Row>
      <Text fontSize="18px" fontWeight={600} color="#c4c4c4" mt={22}>
        Porcentagem de horas por projeto
      </Text>
      <Chart chartType="PieChart" options={options} data={data} />
      <BottomNavigation />
    </Column>
  );
};

export default ProjectsGraphicContainer;
