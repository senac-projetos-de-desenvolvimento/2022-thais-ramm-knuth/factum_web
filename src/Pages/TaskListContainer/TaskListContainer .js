import { useState } from "react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useForm } from "react-hook-form";
import {
  HighlightOff,
  EditSharp,
  LocalOfferSharp,
  Category,
} from "@mui/icons-material";

import {
  Column,
  Header,
  Text,
  Row,
  Link,
  Button,
  Input,
  Loader,
  BottomNavigation,
} from "Shared/Components";
import {
  getAllTasks,
  deleteTask,
  getAllTasksByDate,
} from "Shared/Services/tasks";
import { useUser } from "Shared/Context/userContext";
import { useModal } from "Shared/Context/modalContext";
import { filterByDateSchema } from "Shared/Helpers/yupSchemas";

const TaskListContainer = () => {
  const queryClient = useQueryClient();
  const { handleOpenModal, handleCloseModal } = useModal();

  const { user, userPhoto } = useUser();
  const { id } = user;

  const [tasks, setTasks] = useState([]);

  let { data: alltasks, isFetching } = useQuery(
    ["tasks", id],
    async () => await getAllTasks(id),
    { onSuccess: (data) => setTasks(data) }
  );

  const { mutate: remove, isLoading } = useMutation(deleteTask, {
    onSuccess: () => {
      handleOpenModal({
        type: "success",
        title: "Registro excluído",
        content: "Registro excluído com sucesso!",
      });
      queryClient.invalidateQueries("tasks");
    },
  });

  const handleRemoveNaver = (id) => {
    handleOpenModal({
      type: "confirmation",
      title: "Excluir registro",
      content: "Tem certeza que deseja excluir este registro?",
      onConfirm: () => {
        remove(id);
        handleCloseModal();
      },
    });
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: filterByDateSchema });

  const filterTasks = async (date) => {
    const tasksListByDate = await getAllTasksByDate(id, date);
    setTasks(tasksListByDate);
  };

  const clearTasksFilter = () => setTasks(alltasks);

  return (
    <Column
      bg="#C4C4C4"
      color="#707070"
      height="100vh"
      overflow="auto"
      alignItems="center"
    >
      <Header src={userPhoto} alt="Foto do usuario" variant="light" />
      <Row width={312} height={32} mt={20}>
        <Row as="form" onSubmit={handleSubmit((date) => filterTasks(date))}>
          <Input
            name="date"
            variant="InputDate"
            placeholder="dd/mm/aaaa"
            IsInputDate={true}
            type="string"
            {...register("date")}
            error={errors.date?.message}
          />
          <Button
            type="submit"
            width={45}
            height={33}
            bg="#C4C4C4"
            color="#707070"
            ml="7px"
          >
            <Text fontSize="12px" fontWeight={500}>
              Filtrar
            </Text>
          </Button>
        </Row>
        <Button
          onClick={() => clearTasksFilter()}
          width={45}
          height={33}
          bg="#C4C4C4"
          color="#707070"
          ml="7px"
        >
          <Text fontSize="12px" fontWeight={500}>
            Limpar
          </Text>
        </Button>
      </Row>
      <Row mt={32}>
        {isLoading || isFetching ? (
          <Loader />
        ) : (
          <Column width={312}>
            {tasks?.map(
              ({ id, category, project, durationTime, description }) => (
                <Column key={id} mb={22} alignItems="center">
                  <Row>
                    <Row width={138.5}>
                      <Category sx={{ fontSize: 15 }} />
                      <Text variant="small" ml={1} my="auto" color="#212121">
                        {category.name}
                      </Text>
                    </Row>
                    <Row width={138.5}>
                      <LocalOfferSharp sx={{ fontSize: 13 }} />
                      <Text variant="small" ml={1} my="auto" color="#212121">
                        {project.name}
                      </Text>
                    </Row>
                    <Text variant="small" my="auto" width={35}>
                      {durationTime} h
                    </Text>
                  </Row>
                  <Row justifyContent="space-between" mt={1}>
                    <Row width={248} height={50}>
                      <Text my="auto">{description}</Text>
                    </Row>
                    <Row width={54} mt={30}>
                      <Link
                        to={`/registros/edita/${id}`}
                        color="#338799"
                        mr={2}
                      >
                        <EditSharp />
                      </Link>
                      <Button
                        onClick={() => handleRemoveNaver(id)}
                        color="#338799"
                        variant="transparent"
                      >
                        <HighlightOff />
                      </Button>
                    </Row>
                  </Row>
                  <Row
                    width="100%"
                    mt={2}
                    borderBottom="1px solid rgba(44%, 44%, 44%, 0.25)"
                  />
                </Column>
              )
            )}
          </Column>
        )}
      </Row>
      <BottomNavigation />
    </Column>
  );
};

export default TaskListContainer;
