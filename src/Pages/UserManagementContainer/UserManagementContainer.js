import { useMutation, useQuery, useQueryClient } from "react-query";

import AddIcon from "@mui/icons-material/Add";
import ListIcon from "@mui/icons-material/List";
import { EditSharp } from "@mui/icons-material";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";

import {
  Column,
  Header,
  Link,
  Row,
  Text,
  Button,
  Loader,
  BottomNavigation,
} from "Shared/Components";
import { useUser } from "Shared/Context/userContext";
import { getAllUsers, deleteUser } from "Shared/Services/users";
import { useModal } from "Shared/Context/modalContext";

const UserManagementContainer = () => {
  const { user, userPhoto } = useUser();
  const { handleOpenModal, handleCloseModal } = useModal();
  const queryClient = useQueryClient();

  const { id } = user;

  let { data: usersList, isFetching } = useQuery(
    ["usersList", id],
    async () => await getAllUsers(id)
  );

  const { mutate: remove, isLoading } = useMutation(deleteUser, {
    onSuccess: () => {
      handleOpenModal({
        type: "success",
        title: "Usuário excluído",
        content: "Usuário excluído com sucesso!",
      });
      queryClient.invalidateQueries("usersList");
    },
  });

  const handleRemoveUser = (id) => {
    handleOpenModal({
      type: "confirmation",
      title: "Excluir Usuário",
      content: "Tem certeza que deseja excluir este Uusário?",
      onConfirm: () => {
        remove(id);
        handleCloseModal();
      },
    });
  };

  return (
    <Column
      bg="#808080"
      color="#c4c4c4"
      height="100vh"
      overflow="auto"
      alignItems="center"
    >
      <Header src={userPhoto} alt="Foto do usuario" />
      <Row
        width={312}
        mt={29}
        mb={32}
        alignItems="center"
        justifyContent="space-between"
      >
        <Link to={"/admin/usuarios/cria"} color="#c4c4c4">
          <AddIcon sx={{ fontSize: 25 }} />
          <Text mt="3px">Novo</Text>
        </Link>
        <Text>|</Text>
        <Link to={""} color="#c4c4c4">
          <ListIcon sx={{ fontSize: 25 }} />
          <Text fontWeight={600} mt="3px" ml="2px">
            Todos
          </Text>
        </Link>
      </Row>

      {isFetching || isLoading ? (
        <Loader />
      ) : (
        usersList?.map(({ id, name }) => (
          <Row key={id} justifyContent="space-between">
            <Row height="22px" width={215}>
              <Text fontSize="18px">{name}</Text>
            </Row>
            <Row mb={24} width={78}>
              <Link to={`/admin/usuarios/edita/${id}`}>
                <EditSharp
                  sx={{ color: "#c4c4c4", fontSize: 30, mr: "20px" }}
                />
              </Link>
              <Button
                variant="transparent"
                onClick={() => handleRemoveUser(id)}
              >
                <HighlightOffIcon sx={{ color: "#c4c4c4", fontSize: 30 }} />
              </Button>
            </Row>
          </Row>
        ))
      )}
      <BottomNavigation />
    </Column>
  );
};

export default UserManagementContainer;
