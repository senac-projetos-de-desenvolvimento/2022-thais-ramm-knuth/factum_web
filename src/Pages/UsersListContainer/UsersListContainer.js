import { useQuery } from "react-query";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";

import {
  Column,
  Row,
  Header,
  Loader,
  Image,
  Link,
  BottomNavigation,
} from "Shared/Components";
import { useUser } from "Shared/Context/userContext";
import { getAllUsers } from "Shared/Services/users";

const UsersListContainer = () => {
  const { user, userPhoto } = useUser();

  const { data: users, isFetching } = useQuery(
    ["users"],
    async () => await getAllUsers(),
    {
      refetchOnWindowFocus: true,
    }
  );

  return (
    <Column
      bg="#808080"
      color="#c4c4c4"
      height="100vh"
      overflow="auto"
      alignItems="center"
    >
      <Header src={userPhoto} alt="Foto do usuario" />
      <Row my={12} />

      {isFetching ? (
        <Loader />
      ) : (
        <Row width={296} flexWrap="wrap" gridColumnGap={43}>
          {user.role === "ROLE_ADMIN" && (
            <Link to={"/admin/usuarios/cria"}>
              <Row
                width={70}
                height={70}
                bg="#999999"
                borderRadius="100%"
                boxShadow="0px 2px 3px 0px rgba(0, 0, 0, 0.5)"
                alignItems="center"
                mb={24}
              >
                <ManageAccountsIcon
                  sx={{ ml: 2, fontSize: 45, color: "#338799" }}
                />
              </Row>
            </Link>
          )}
          {users?.map(({ id, name, picture, availabilityStatus }) => (
            <Column key={id} mb={12}>
              <Image
                src={picture}
                alt={`Foto do usuário ${name}`}
                isAvailability={availabilityStatus}
              />
            </Column>
          ))}
        </Row>
      )}
      <BottomNavigation />
    </Column>
  );
};

export default UsersListContainer;
