# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `Sobre o projeto`
Detalhes técnicos estão na [Wiki](https://gitlab.com/senac-projetos-de-desenvolvimento/2022-thais-ramm-knuth/factum_doc_version_2/-/wikis/home) e no repositório da API: [factum_api](https://gitlab.com/senac-projetos-de-desenvolvimento/2022-thais-ramm-knuth/factum_api).

